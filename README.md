# Storylike Issue Board

> The biggest room in the world  
> is the room for improvement.
> 
> -Helmut Schmidt
---

Add new issues and track when we fixed them here:
https://gitlab.com/storylike/public/issue-board/issues